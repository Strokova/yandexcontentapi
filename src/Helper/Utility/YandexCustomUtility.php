<?php


namespace App\Helper\Utility;


class YandexCustomUtility
{
    /**
     * @param array $array1
     * @param array $array2
     * @return array
     */
    public static function arrayAppend(array $array1, array $array2)
    {
        foreach($array2 as $k){
            array_push($array1, $k);
        }

        return $array1;
    }

    public static function getCategories($categoriesList)
    {
        $array = [];
        $data = [];
        $data['id'] = $categoriesList->vendor->id;
        $data['name'] = $categoriesList->vendor->name;
        foreach ($categoriesList->vendor->categories as $k) {
            foreach ($k->children as $key) {
                $elem['id'] = $key->id;
                $elem['fullName'] = $key->fullName;
                $array[] = $elem;
            }
        }
        $data['categories'] = $array;

        return $data;
    }
}