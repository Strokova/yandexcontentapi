<?php

namespace App\Helper\Converter;

/**
 * Class ResponseConverter
 * @package App\Helper\Converter
 */
class ResponseConverter
{
    /**
     * @param array $data
     * @param string $status
     * @param array $meta
     * @return array
     */
    public function response(array $data, string $status)
    {
        $response = [
            'status' => $status,
            'response' => $data
        ];

        return $response;
    }

    public function exception(string $message, string $code): array
    {
        return [
            'error_message' => $message,
            'error_code' => $code
        ];
    }
}