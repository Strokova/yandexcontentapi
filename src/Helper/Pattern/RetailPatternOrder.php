<?php


namespace App\Helper\Pattern;


class RetailPatternOrder implements OrderInterface
{

    public function setOrderTemp(array $data): array
    {
        $order = [
            'number' => '',
            'externalId' => '',
            'countryIso' => '',
            'createdAt' => '',
            'lastName' => '',
            'firstName' => '',
            'patronymic' => '',
            'phone' => '',
            'additionalPhone' => '',
            'email' => '',
            'call' => '',
            'customerComment' =>'',
            'managerComment' => '',

            'weight' => '',
            'length' => '',
            'width' => '',
            'height' => '',

            'customFields' => [],

            'orderType' => '',
            'orderMethod' => '',

            'status' => '',

            'items' => [],

            'delivery' => [],
        ];

        return $order;
    }

    public function getOrderTemp(int $orderId): array
    {
        // TODO: Implement getOrderTemp() method.
    }

    public function hasOrderTemp(int $orderId): array
    {
        // TODO: Implement hasOrderTemp() method.
    }
}