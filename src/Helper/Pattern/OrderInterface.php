<?php


namespace App\Helper\Pattern;


interface OrderInterface
{
    /**
     * @param array $data
     * @return array
     *
     * This method install schema order for provider
     */
    public function setOrderTemp(array $data):array ;

    /**
     * @param int $orderId
     * @return array
     *
     * This method getter of order from database
     */
    public function getOrderTemp(int $orderId):array ;

    /**
     * @param int $orderId
     * @return array
     *
     * This method of order verification exists or not
     */
    public function hasOrderTemp(int $orderId):array ;
}