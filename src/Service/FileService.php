<?php


namespace App\Service;


use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\IReader;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\IWriter;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;

class FileService
{
    /**
     * @var string $path
     */
    protected $path = 'data/ForRead/';

    /**
     * @var string $fileName
     */
    protected $fileName;

    /**
     * @var string $type
     */
    protected $type;

    /**
     * @var array $typeList
     */
    protected $typeList = ['Xls', 'Xlsx', 'Xml', 'Html', 'Ods', 'Slk', 'Gnumeric', 'Csv'];

    public function __construct(string $fileName = '', string $type = '')
    {
        $this->fileName = $fileName;
        $this->type = $type;
    }


    /**
     * @param Spreadsheet $spreadsheet
     * @param string $iteration
     * @param int $row
     * @return array
     * @throws Exception
     */
    public function getRow(Spreadsheet $spreadsheet, string $iteration, int $row)
    {
        $value = [];

        if($this->checkExists()){
            $cells = $spreadsheet->getActiveSheet();

            for($col = 1; $col <= $iteration; $col++){
                $value[] = $cells->getCellByColumnAndRow($col, $row)->getValue();
            }
        }else{
            throw new FileNotFoundException($this->path . $this->fileName);
        }

        return $value;
    }

    /**
     * @param $data
     * @param $type
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function setData($data, $type)
    {
        $spreadsheet = $this->createSpreadsheet();
        $writer = $this->createWriter($spreadsheet,$type);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->fromArray($data,NULL,'A1');
        $writer->save('data/result' . date("Y-m-d H:i:s") . '.xlsx');
    }

    /**
     * @param Spreadsheet $spreadsheet
     * @return int
     * @throws Exception
     */
    public function getLastRow(Spreadsheet $spreadsheet)
    {
        return $spreadsheet->getActiveSheet()->getHighestRow();
    }

    /**
     * @return bool
     */
    private function checkExists()
    {
        $result = false;

        if(file_exists($this->fileName)){
            $result = true;
        }
        return $result;
    }

    /**
     * @return IReader
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function createReader()
    {
        return IOFactory::createReader($this->type)->setReadDataOnly(true);
    }

    /**
     * @return Spreadsheet
     */
    private function createSpreadsheet()
    {
        return new Spreadsheet();
    }

    /**
     * @param $spreadsheet
     * @param $type
     * @return IWriter
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    private function createWriter($spreadsheet, $type)
    {
        return IOFactory::createWriter($spreadsheet, $type);
    }
}