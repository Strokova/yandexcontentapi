<?php


namespace App\Service;


use Psr\Log\LoggerInterface;

class LogService
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * LogService constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param array $data
     */
    public function log($data)
    {
        switch ($data['level']){
            case -1: $this->logger->error($data['message']) ;
            break;

            case 1: $this->logger->notice($data['message']) ;
            break;

            case 2: $this->logger->info($data['message']) ;
            break;

            default: ;
        }
    }
}