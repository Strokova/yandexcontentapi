<?php


namespace App\Service;



class LoggerService
{

    /**
     * @var $instance
     */
    private static $instance;

    /**
     * LoggerService constructor.
     */
    private function __construct() { }

    /**
     * @return Logger
     */
    public static function getInstance() {

        if(!self::$instance) {
            self::$instance = '';
        }


        return self::$instance;
    }

    private function __wakeup() { }

    private function __clone() { }
}