<?php


namespace App\Service;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BasicService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * PageService constructor.
     * @param EntityManagerInterface $manager
     * @param ValidatorInterface $validator
     * @param SerializerInterface $serializer
     */
    public function __construct(EntityManagerInterface $manager,
                                ValidatorInterface $validator,
                                SerializerInterface $serializer
    )
    {
        $this->em = $manager;
        $this->validator = $validator;
        $this->serializer = $serializer;
    }


    public function create($data)
    {

        try {

            $violations = $this->validator->validate($data);
            if ($violations->count() > 0) {
                $this->em->detach($data);
                throw new \Exception('Error, invalid data request', 500);
            }

            $this->em->persist($data);
            $this->em->flush();

            $content = $this->serializer->serialize($data, 'json', []);
            return json_decode($content, true);

        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage(), 500);
        }
    }

    public function update($data)
    {
        try {
            $violations = $this->validator->validate($data);
            if ($violations->count() > 0) {
                $this->em->detach($data);
                throw new \Exception('Error, invalid data request', 500);
            }
//            var_dump($violations);
//            var_dump($data);
            $this->em->merge($data);
            $this->em->flush();
            $content = $this->serializer->serialize($data, 'json', []);

            return json_decode($content, true);

        } catch (\Exception $exception) {
            throw new \Exception('Error SQL query, invalid data request', 500);
        }
    }

}