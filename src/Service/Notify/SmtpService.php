<?php


namespace App\Service\Notify;

use \Swift_Mailer;
use \Swift_Message;

class SmtpService
{
    private $smtp;

    public function __construct(Swift_Mailer $mailer)
    {
        $this->smtp = $mailer;
    }

    public function sand()
    {
        $message = (new Swift_Message("Test hello"))
            ->setFrom('nazarov@fim.ltd')
            ->setTo('nazarov@fim.ltd')
            ->setBody('hello', 'text/html')
        ;

        return $this->smtp->send($message);
    }
}