<?php


namespace App\Service;

use App\Exception\NotFoundException;
use App\Exception\YandexException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;

class YandexService
{
    /**
     * @var string $key
     */
    protected $key;

    /**
     * @var string $host
     * Required parameter
     */
    protected $host = 'api.content.market.yandex.ru';

    /**
     * @var string $version
     * Required parameter
     */

    protected $version = 'v2';

    /**
     * Array for params and methods validation in get requests
     * @var array $alias
     */
    protected $alias = [
        'categories_all' => [
            'path' => 'categories',
            'require' => ['geo_id']
        ],
        'category_info' => [
            'path' => 'categories/%s',
            'require' => ['geo_id']
        ],
        'category_filters' => [
            'path' => 'categories/%s/filters',
            'require' => ['geo_id']
        ],
        'categories_child' => [
            'path' => 'categories/%s/children',
            'require' => ['geo_id']
        ],
        'geo_all' => [
            'path' => 'geo/regions',
            'require' => []
        ],
        'geo_child' => [
            'path' => 'geo/regions/%s/children',
            'require' => []
        ],
        'vendors_all' => [
            'path' => 'vendors',
            'require' => ['geo_id']
        ],
        'vendor_search' => [
            'path' => 'vendors/match',
            'require' => ['geo_id', 'name']
        ],
        'vendor_categories' => [
            'path' => 'vendors/match',
            'require' => ['geo_id', 'name', 'fields']
        ],
        'models_all' => [
            'path' => 'categories/%s/search',
            'require' => ['geo_id', 'result_type', '-11', 'fields'],
        ],
        'models_search' => [
            'path' => 'categories/%s/search',
            'require' => ['geo_id', 'result_type', '-8', 'fields'],
        ],
        'models_match' => [
            'path' => 'models/match',
            'require' => ['name']
        ],
        'model_info' => [
            'path' => "models/%s",
            'require' => ['geo_id']
        ],
        'search' => [
            'path' => 'search',
            'require' => ['geo_id', 'text']
        ],
    ];

    public function __construct($key)
    {
        $this->key = $key;
    }

    /**
     * Set params for request to yandex
     * @param array $requestParams
     * @param string $queryUri
     * @param int|null $id
     * @param int|null $page
     * @param string|null $name
     * @return mixed
     * @throws NotFoundException
     * @throws YandexException
     */
    public function setParams(array $requestParams, string $queryUri, int $id = null, int $page = null, string $name = null)
    {
        try{

            $params['key'] = $this->key;
            $params['uri'] = $this->getUri($queryUri, $id);

            if (!empty($page)) {
                $params['content']['page'] = $page;
            }

            if (!empty($name)) {
                $params['content']['name'] = $name;
            }
            $params['content']['count'] = 30;
            $params['content'] = $params['content'] + $requestParams;

            $this->validateParams($queryUri, $params);

        }catch (YandexException $exception){
            throw new YandexException($exception->getMessage(), $exception->getCode());
        }

        return $params;
    }


    /**
     * @param array $params
     * @return string
     * @throws GuzzleException
     */
    public function makeRequest(array $params)
    {

        $client = new Client([
            'base_uri' => 'https://' . $this->host . '/' . $this->version . '/',
        ]);

        try {
            $response = $client->request('GET', $params['uri'], [
//                'debug' => true,
                'headers' => [
                    'Authorization' => $params['key'],
                    'Host' => $this->host,
                    'Accept' => '*/*',
                ],
                'query' => $params['content'],
            ]);

        } catch(ServerException  $e) {
            return $this->makeRequest($params);
        }

        return (string)$response->getBody()->getContents();
    }



    /**
     * Select part of the uri-path to yandex api methods
     * @param string $queryUri
     * @param int|null $id
     * @return mixed|string
     * @throws NotFoundException
     */
    private function getUri(string $queryUri, int $id = null)
    {

        if (key_exists($queryUri, $this->alias)) {
            $uri = $this->alias[$queryUri]['path'];
        } else {
            throw new NotFoundException('Page \'' . $queryUri . '\' not found.');
        }

        if (in_array($queryUri, ['category_info', 'category_filters', 'categories_child', 'geo_child', 'models_all', 'models_search', 'model_info'])) {
            if(!empty($id)){
                $uri = sprintf($uri, $id);
            }else{
                throw new NotFoundException("Require param 'id' are missing.");
            }

        }

        return $uri;
    }

    /**
     * validate require params in get request
     * @param $queryUri
     * @param $params
     * @throws NotFoundException
     * @throws YandexException
     */
    private function validateParams($queryUri, $params)
    {
        try{
            $source = array_flip($this->alias[$queryUri]['require']);
            if(!empty(array_diff_key($source,$params['content']))){
                $missing = array_diff_key($source,$params['content']);
                throw new NotFoundException('Require param(s) are missing: ' . implode(',', array_keys($missing)));
            }
        }catch(YandexException $exception){
            throw new YandexException($exception->getMessage(), $exception->getCode());
        }
    }


}