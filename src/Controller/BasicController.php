<?php


namespace App\Controller;

use App\Helper\Converter\ResponseConverter;
use App\Service\BasicService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

class BasicController implements BasicControllerInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $manager;

    /**
     * @var ResponseConverter
     */
    protected $response;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var BasicService
     */
    protected $service;

    /**
     * @var object
     *
     * Empty default value, this is an entity class
     */
    protected $type;

    public function __construct(
        EntityManagerInterface $manager,
        ResponseConverter $response,
        SerializerInterface $serializer,
        BasicService $service
    ) {

        if(empty($this->type))
            throw new \Exception('Warring: install entity class to protected:TYPE', 500);

        $this->manager = $manager;
        $this->response = $response;
        $this->serializer = $serializer;
        $this->service = $service;
    }

    public function deserialize(Request $request): object
    {
        return $this->serializer->deserialize($request->getContent(), $this->type, $request->getContentType());
    }

    public function normalize(object $data): array
    {
        $result = $this->serializer->serialize($data, 'json');

        return json_decode($result, true);
    }

    public function response(array $data): JsonResponse
    {
        /**
         * Install response status
         */
        $status = (array_key_exists('error_message', $data)) ? 'error' : 'success';

        /**
         * Install response data
         */
        $result = $this->response->response((array)$data, $status);

        return new JsonResponse($result, 200);
    }
}