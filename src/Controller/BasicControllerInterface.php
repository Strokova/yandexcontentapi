<?php


namespace App\Controller;


use Symfony\Component\HttpFoundation\Request;

interface BasicControllerInterface
{
    /**
     * @param Request $request
     * @return object
     *
     * Method for converting request content to entity object
     */
    public function deserialize(Request $request): object ;

    /**
     * @param object $data
     * @return array
     *
     * The method of converting an entity object to an array
     */
    public function normalize(object $data): array ;
}