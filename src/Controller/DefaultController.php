<?php


namespace App\Controller;

use App\Service\Notify\SmtpService;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route as SeRoute;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Debug\Exception\FlattenException;
/**
 * Class DefaultController
 *
 * @SeRoute("", service="app.controller.default")
 */
class DefaultController
{

    private $notify;

    public function __construct(SmtpService $notify)
    {
        $this->notify = $notify;
    }


    public function showAction(Request $request, FlattenException $exception, LoggerInterface $logger)
    {
        /**
         * Get HTTP status code
         */
        $code = $exception->getStatusCode();
        /**
         * Response message for client
         */
        $message = [
            'status' => 'error',
            'response' => [
                'error_status' => isset(Response::$statusTexts[$code]) ? Response::$statusTexts[$code] : '',
                'error_message' => $exception->getMessage(),
                'error_code' => $code
            ]
        ];

        /**
         * Create logger
         */
        $logger->warning($message['response']['error_message'],['code' => $code]);


        /**
         * Send email notify
         */
       // $mail = $this->notify->sand();

        return new JsonResponse($message, $code,
            [
                'Content-Type' => 'application/json'
            ],
        false
        );
    }

    public function accessError(Request $request)
    {
        return new JsonResponse([
            'status' => 'protection',
            'response' => [
                'message' => 'Forbidden',
                'code' => 403
            ]
        ], 403,
            [
                'Content-Type' => 'application/json'
            ]
        );
    }
}