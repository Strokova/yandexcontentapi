<?php


namespace App\Controller\Yandex;


use App\Exception\YandexException;
use App\Helper\Utility\YandexCustomUtility;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CategoriesController extends YandexController
{

    public function __construct()
    {

    }

    /**
     * @Route(path="/yandex/categories/{queryString}/{id}", methods={"GET"}, requirements={"queryString": "categories_all|category_info|category_filters|categories_child", "id":"^(\d+$)"})
     * @param Request $request
     * @param string|null $queryString
     * @param int|null $id
     * @return JsonResponse
     * @throws YandexException
     * @throws GuzzleException
     */
    public function get(Request $request, string $queryString = null, int $id = null)
    {

        try {
            $data = $this->getItems($request, $queryString, $id);

        } catch (YandexException $exception) {
            throw new YandexException($exception->getMessage(), $exception->getCode());
        }
        return new JsonResponse(json_decode($data), 200);
    }

    /**
     * @Route(path="/yandex/categories/categories_rec", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     * @throws YandexException
     * @throws GuzzleException
     */
    public function getAll(Request $request)
    {
        try {
            $queryString = 'categories_all';
            $data = json_decode((string)$this->getItems($request, $queryString));
            $array = $this->getCategories($request, $data);

        } catch (YandexException $exception) {
            throw new YandexException($exception->getMessage(), $exception->getCode());
        }

        return new JsonResponse($array, 200);
    }

    /**
     * @param Request $request
     * @param object $items
     * @return array
     * @throws YandexException
     * @throws GuzzleException
     */
    private function getCategories(Request $request, object $items)
    {
        $queryString = 'categories_child';
        $parsedItems = [];

        if (!empty($items)) {

            while ($item = array_shift($items->categories)) {
                if ($item->childCount > 0) {
                    $items->categories = YandexCustomUtility::arrayAppend($items->categories, json_decode((string)$this->getItems($request, $queryString, $item->id))->categories);
                }
                $parsedItems[] = $this->prepareData($item);
            }

        }

        return $parsedItems;
    }

    /**
     * @param object $data
     * @return mixed
     */
    private function prepareData(object $data)
    {

        $elem['name'] = $data->fullName;
        $elem['parent'] = $data->parent;
        $elem['id'] = $data->id;

        return $elem;
    }
}