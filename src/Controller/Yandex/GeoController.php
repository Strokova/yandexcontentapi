<?php


namespace App\Controller\Yandex;


use App\Exception\YandexException;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class GeoController extends YandexController
{
    /**
     * @Route(path="/yandex/geo/{queryString}/{id}", methods={"GET"})
     * @param Request $request
     * @param string|null $queryString
     * @param int|null $id
     * @return JsonResponse
     * @throws YandexException
     * @throws GuzzleException
     */
    public function get(Request $request, string $queryString = null, int $id = null)
    {
        try{
            $data = $this->getItems($request, $queryString, $id);
        }catch(YandexException $exception){
            throw new YandexException($exception->getMessage(),$exception->getCode());
        }
        return new JsonResponse(json_decode($data), 200);
    }
}