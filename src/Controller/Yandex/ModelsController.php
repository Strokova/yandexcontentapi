<?php


namespace App\Controller\Yandex;


use App\Exception\YandexException;
use App\Helper\Utility\YandexCustomUtility;
use App\Service\FileService;
use GuzzleHttp\Exception\GuzzleException;
use PhpOffice\PhpSpreadsheet\Writer\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ModelsController extends YandexController
{

    private $preData = [];

    /**
     * @var int
     */
    protected $iterations = 1;

    protected $log;

    public function __construct()
    {
    }

    /**
     * @Route(path="/yandex/models/{queryString}/{id}", methods={"GET"}, requirements={"queryString": "models_search|model_info", "id":"^(\d+$)"})
     * @param Request $request
     * @param string|null $queryString
     * @param int|null $id
     * @return JsonResponse
     * @throws YandexException
     * @throws GuzzleException
     */
    public function get(Request $request, string $queryString = null, int $id = null)
    {
        try {
            $data = $this->getItems($request, $queryString, $id);
        } catch (YandexException $exception) {
            throw new YandexException($exception->getMessage(), $exception->getCode());
        }

        return new JsonResponse(json_decode($data), 200);
    }

    /**
     * @Route(path="/yandex/models/models_match", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     * @throws GuzzleException
     * @throws YandexException
     */
    public function match(Request $request)
    {
        try {
            $queryString = 'models_match';
            $data = $this->getItems($request, $queryString);
            $result = $this->prepareDataMutch(json_decode($data));
        } catch (YandexException $exception) {
            throw new YandexException($exception->getMessage(), $exception->getCode());
        }

        return new JsonResponse($result, 200);
    }

    /**
     * @Route(path="/yandex/models/models_all/{id}", methods={"GET"})
     * @param Request $request
     * @param int|null $id
     * @return JsonResponse
     * @throws YandexException
     * @throws GuzzleException
     */
    public function getAll(Request $request, int $id = null)
    {
        try {
            $brand = '';
            $queryString = 'models_all';

            for ($i = 1; $i <= $this->iterations; $i++) {

                $result = json_decode($this->getItems($request, $queryString, $id, $i));
                $brand = $result->items[0]->vendor->name ?? $brand;
                $this->prepareData($result);

                if ($result->context->page->total >= 50) {
                    $this->iterations = 50;
                } else {
                    $this->iterations = $result->context->page->total;
                }
            }

            $data['brand'] = $brand;
            $data['items'] = $this->preData;

            $this->makeFile($data);

        } catch (YandexException $exception) {
            throw new YandexException($exception->getMessage(), $exception->getCode());
        }

        return new JsonResponse(json_decode(json_encode($data), JSON_PRETTY_PRINT), 200);
    }

    /**
     * @Route(path="/yandex/models/models_file", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     * @throws GuzzleException
     * @throws YandexException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws Exception
     */
    public function fromFile(Request $request)
    {

        $queryString = 'models_match';
        $models = [];
        try {

            $fcon = new FileService('data/ForRead/tmall.xlsx', 'Xlsx'); //$fileName
            $reader = $fcon->createReader();
            $spreadsheet = $reader->load('data/ForRead/tmall.xlsx');        //$fileName
            $maxRow = $fcon->getLastRow($spreadsheet);
            $testArray2 = [];

            for ($i = 2; $i <= $maxRow; $i++) {
                $names = $fcon->getRow($spreadsheet, 8, $i);

                if (($i % 10) == 0) {
                    sleep(2);
                }

                $match = json_decode($this->getItems($request, $queryString, null, null, $names[3]));
                if (count($match->models) == 1) {
                    $models[] = $this->fillData($names, $match->models[0]);
                } else {
                    $testArray2 [] = $names[3];
                }
            }

            $fcon->setData($models, 'Xlsx');
            unset($fcon);

        } catch (YandexException $exception) {
            throw new YandexException($exception->getMessage(), $exception->getCode());
        }

        return new JsonResponse(json_decode(json_encode($models), JSON_PRETTY_PRINT), 200);
    }

    /**
     * @Route(path="/yandex/models/models_all2", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     * @throws YandexException
     * @throws GuzzleException
     */
    public function getModelsCategories(Request $request)
    {

        $queryString = 'vendor_categories';
        $filter = $request->query->get('-11');
        $request->query->remove('-11');
        $request->query->set('count', '30');

        try {
            $categoriesList = json_decode($this->getItems($request, $queryString));
            $data = $data = YandexCustomUtility::getCategories($categoriesList);
            $request = $this->changeRequest($request, $filter);

            $brand = '';
            $itemsPart = [];
            $queryString = 'models_all';

            foreach ($data['categories'] as $k) {
                $this->iterations = 1;
                for ($i = 1; $i <= $this->iterations; $i++) {

                    $result = json_decode($this->getItems($request, $queryString, $k['id'], $i));
                    $brand = $result->items[0]->vendor->name ?? $brand;
                    $this->prepareData($result);
                    $this->countPages($result->context->page->total);
                }
            }

            $itemsPart['brand'] = $brand;
            $itemsPart['items'] = $this->preData;

            $this->makeFile($itemsPart);

        } catch (YandexException $exception) {
            throw new YandexException($exception->getMessage(), $exception->getCode());
        }

        return new JsonResponse($itemsPart, 200);
    }

    /**
     * @param Request $request
     * @param $filter
     * @return Request
     */
    private function changeRequest(Request $request, $filter)
    {
        $request->query->set('-11', $filter);
        $request->query->remove('fields');
        $request->query->remove('name');
        $request->query->remove('count');
        $request->query->set('fields', 'MODEL_VENDOR');
        $request->query->set('result_type', 'MODELS');
        $request->query->set('count', '30');

        return $request;
    }

    /**
     * @param $data
     * @return array
     */
    private function prepareDataMutch($data)
    {
        $result = [];
        foreach($data->models as $k){
            $result[] = $k->id;
        }

        return $result;
    }

    /**
     * @param $total
     */
    private function countPages($total)
    {
        if ($total >= 50) {
            $this->iterations = 50;
        } else {
            $this->iterations = $total;
        }
    }

    /**
     * @param $result
     * @return array
     */
    private function prepareData($result)
    {
        foreach ($result->items as $k => $v) {
            $elem['id'] = $v->id;
            $elem['name'] = $v->name;
            $this->preData[] = $elem;

        }

        return $this->preData;
    }

    /**
     * @param array $names
     * @param object|null $data
     * @return mixed
     */
    private function fillData(array $names, object $data = null)
    {
        $elem['code'] = $names[0];
        $elem['art'] = $names[1];
        $elem['uuid'] = $names[2];
        $elem['name'] = $names[3];
        $elem['group'] = str_replace('>', '\\', $names[4]);
        $elem['cost'] = $names[5];
        $elem['weight'] = $names[6];
        $elem['group2'] = $names[7];
        $elem['yid'] = $data->id ?? null;
        $elem['yname'] = $data->name ?? null;

        return $elem;
    }
}