<?php


namespace App\Controller\Yandex;

use App\Exception\YandexException;
use App\Helper\Utility\YandexCustomUtility;
use App\Service\YandexService;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;


class YandexController
{

    /**
     * @param Request $request
     * @param string $queryUri
     * @param int|null $id
     * @param int|null $page
     * @param string|null $name
     * @return string
     * @throws YandexException
     * @throws GuzzleException
     */
    public function getItems(Request $request, string $queryUri, int $id = null, int $page = null, string $name = null)
    {
        try {
            $service = new YandexService('KT4CB9MDBKRVJ7UDMOCCZWKLIZSBBN');
            $params = $service->setParams($request->query->all(), $queryUri, $id, $page, $name);

            $data = $service->makeRequest($params);
        } catch (\Exception $exception) {

            throw new YandexException();
        }

        return $data;
    }

    /**
     * Write json into file
     * @param $data
     */
    public function makeFile($data)
    {
        try {
            $fileName = "data/". (string)$data['brand'] . ".json";
            $dataWrite = [];

            if (file_exists($fileName)) {
                if(!empty(file_get_contents($fileName))) {
                    $dataWrite = json_decode(file_get_contents($fileName),true);
                    $dataWrite['items'] = YandexCustomUtility::arrayAppend($dataWrite["items"], $data["items"]);

                    $dataWrite = json_encode($dataWrite, JSON_PRETTY_PRINT);
                }
            }else{
                $dataWrite = json_encode($data,JSON_PRETTY_PRINT);
            }
            file_put_contents($fileName,$dataWrite, LOCK_EX);
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage(), $exception->getCode());
        }
    }
}