<?php


namespace App\Controller\Yandex;


use App\Exception\YandexException;
use App\Helper\Utility\YandexCustomUtility;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class VendorsController extends YandexController
{
    /**
     * array of vendors
     * @var array $vendors
     */
    protected $vendors = [];

    /**
     * max count of pages in responce
     * @var int $page
     */
    protected $page = 1;

    /**
     * @Route(path="/yandex/vendors/{queryString}/{id}", methods={"GET"}, requirements={"queryString": "vendor_search", "id":"^(\d+$)"})
     * @param Request $request
     * @param string|null $queryString
     * @param int|null $id
     * @return JsonResponse
     * @throws YandexException
     * @throws GuzzleException
     */
    public function get(Request $request, string $queryString = null, int $id = null)
    {
        try {
            $data = $this->getItems($request, $queryString, $id);
        } catch (YandexException $exception) {
            throw new YandexException($exception->getMessage(), $exception->getCode());
        }
        return new JsonResponse(json_decode($data), 200);
    }

    /**
     * @Route(path="/yandex/vendors/vendors_all", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     * @throws YandexException
     * @throws GuzzleException
     */
    public function getAll(Request $request)
    {
        $queryString = 'vendors_all';

        try {
            for ($i = 1; $i <= $this->page; $i++) {
                $result = json_decode($this->getItems($request, $queryString, null, $i));

                $this->countPages($result->context->page->total, 30);

                $this->prepareData($result);

            }
        } catch (YandexException $exception) {
            throw new YandexException($exception->getMessage(), $exception->getCode());
        }
        return new JsonResponse($this->vendors, 200);
    }

    /**
     * @Route(path="/yandex/vendors/vendor_categories", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     * @throws YandexException
     * @throws GuzzleException
     */
    public function getVendorCategories(Request $request)
    {

        $queryString = 'vendor_categories';

        try {
            $categoriesList = json_decode($this->getItems($request, $queryString));

            $data = YandexCustomUtility::getCategories($categoriesList);

            $data = json_encode($data);
        } catch (YandexException $exception) {
            throw new YandexException($exception->getMessage(), $exception->getCode());
        }

        return new JsonResponse(json_decode($data), 200);
    }

    /**
     * @param $result
     * @return mixed
     */
    private function prepareData($result)
    {
        foreach ($result->vendors as $k => $v) {
            $elem['id'] = $v->id;
            $elem['name'] = $v->name;
            $this->vendors[] = $elem;
        }

        return $this->vendors;
    }

    private function countPages($total, $count)
    {
        $number = $total / $count;
        if($total % $count != 0){
            $number = floor($number) + 1;
        }

        $this->page = $number;
    }
}