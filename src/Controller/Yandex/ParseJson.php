<?php


namespace App\Controller\Yandex;


use App\Exception\YandexException;
use App\Service\FileService;
use PhpOffice\PhpSpreadsheet\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ParseJson
{

    protected $array = [];

    /**
     * @Route( methods={"GET"}, path="/yandex/parse")
     * @return JsonResponse
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function parseFile()
    {
        $data = file_get_contents('data/ForRead/BD-aliexpress.json');

        $data = json_decode($data);

        $fcon = new FileService('data/test.xlsx', 'Xlsx');

        $this->prepareData($data);

        $fcon->setData($this->array, 'Xlsx');
        unset($fcon);

        return new JsonResponse($data, 200);
    }

    private function prepareData($data)
    {
        foreach ($data as $k => $v) {
            if (isset($v->item)) {
                foreach ($v->item->sku as $key) {
                    $elem['item_id'] = $v->item->id;
                    $elem['item_category_id'] = $v->item->category_id;
                    $elem['item_group_id'] = $v->item->group_id;
                    $elem['item_name'] = $v->item->name;
                    $elem['item_sku_uuid'] = $key->uuid;
                    $elem['item_sku_id'] = $key->id;
                    $elem['item_sku_price'] = $key->price;
                    $elem['item_sku_stock'] = $key->stock;
                    $this->array[] = $elem;
                }
            }
        }

    }
}