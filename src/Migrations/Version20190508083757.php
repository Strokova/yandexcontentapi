<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190508083757 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE answer_catalog_brands_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE answer_catalog_products_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE answer_settings_vendor_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE answer_settings_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE answer_settings_seller_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE answer_managers_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE answer_catalog_brands (id INT NOT NULL, header VARCHAR(256) NOT NULL, name VARCHAR(256) NOT NULL, meta_title VARCHAR(256) NOT NULL, meta_keywords VARCHAR(256) NOT NULL, meta_description VARCHAR(256) NOT NULL, href VARCHAR(128) NOT NULL, description TEXT NOT NULL, date_create TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, date_update TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, visible BOOLEAN DEFAULT \'true\' NOT NULL, position INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE answer_catalog_products (id INT NOT NULL, uuid VARCHAR(64) NOT NULL, header VARCHAR(256) NOT NULL, name VARCHAR(256) NOT NULL, brand_id INT NOT NULL, model VARCHAR(128) NOT NULL, meta_title VARCHAR(256) NOT NULL, meta_keywords VARCHAR(256) NOT NULL, meta_description VARCHAR(256) NOT NULL, href VARCHAR(128) NOT NULL, description TEXT NOT NULL, annotation TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE answer_settings_vendor (id INT NOT NULL, name VARCHAR(128) NOT NULL, brand_name VARCHAR(256) NOT NULL, nickname VARCHAR(256) NOT NULL, password VARCHAR(256) NOT NULL, token VARCHAR(512) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE answer_settings (id INT NOT NULL, name VARCHAR(255) NOT NULL, data TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE answer_settings_seller (id INT NOT NULL, name VARCHAR(128) NOT NULL, code VARCHAR(2) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE answer_managers (id INT NOT NULL, username VARCHAR(180) NOT NULL, roles TEXT NOT NULL, password VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8E03E163F85E0677 ON answer_managers (username)');
        $this->addSql('COMMENT ON COLUMN answer_managers.roles IS \'(DC2Type:json)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE answer_catalog_brands_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE answer_catalog_products_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE answer_settings_vendor_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE answer_settings_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE answer_settings_seller_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE answer_managers_id_seq CASCADE');
        $this->addSql('DROP TABLE answer_catalog_brands');
        $this->addSql('DROP TABLE answer_catalog_products');
        $this->addSql('DROP TABLE answer_settings_vendor');
        $this->addSql('DROP TABLE answer_settings');
        $this->addSql('DROP TABLE answer_settings_seller');
        $this->addSql('DROP TABLE answer_managers');
    }
}
