### Symfony Skeleton ###
``current version 1.0``

# Before launch 

**Методы**

1) **categories_all**  
    - ```categories_all``` - получение списка родительских категорий. <br>
    - Параметры: geo_id.<br>
    - Пример запроса: ```yandex/categories/categories_all?geo_id=geo_id```

2) **category_info** 
    - ```category_info``` - подробная информация по категории. <br>
    - Параметры: geo_id. id категории используется в uri.<br>
    - Пример запроса: ```yandex/categories/category_info/%category_id%?geo_id=geo_id```

3) **category_filters**
    - ```category_filters``` - получение списка возможных фильтров для подбора по параметрам в категории. <br>
    - Параметры: geo_id. id категории используется в uri.<br>
    - Пример запроса: ```yandex/categories/category_filters/%category_id%?geo_id=geo_id```

4) **category_filters**
    - ```categories_child``` - получение дочерних категорий по указанной категории.<br>
    - Параметры: geo_id. id категории используется в uri.<br>
    - Пример запроса: ```yandex/categories/categories_child/%category_id%?geo_id=geo_id```

5) **categories_rec**
    - ```categories_rec``` - получение дерева категорий. <br>
    - Параметры: geo_id, fields.<br>
    - Пример запроса: ```yandex/categories/categories_rec?geo_id=geo_id&fields=PARENT```

6) **geo_all**
    - ```geo_all``` - получение всех регионов.<br>
    - Параметры: без параметров.<br>
    - Пример запроса: yandex/geo/geo_all

7) **geo_child**
    - ```geo_child``` - получение дочерних регионов.<br>
    - Параметры: без параметров. id региона используется в uri.<br>
    - Пример запроса: ```yandex/geo/geo_child/%geo_id%```

8) **vendors_all**
    - ```vendors_all``` - получение списка производителей.<br>
    - Параметры: geo_id.<br>
    - Пример запроса: ```yandex/vendors/vendors_all?geo_id=geo_id```

9) **vendor_search**
    - ```vendor_search``` - поиск производителя по наименованию.<br>
    - Параметры: geo_id, name.<br>
    - Пример запроса: ```yandex/vendors/vendor_search?geo_id=geo_id&name=%vendor_name%```

10) **vendor_categories**
    - ```vendor_categories``` - получение всех категорий в которых представлен производитель.<br>
    - Параметры: geo_id, name, fields.<br>
    - Пример запроса: ```yandex/vendors/vendor_categories?geo_id=geo_id&name=%vendor_name%&fields=categories```

11) **models_all**
    - ```models_all``` - получение списка моделей. (стр. 1 - 50)<br>
    - Параметры: geo_id, result_type, -11 (фильтр по производителю), fields. id категории используется в uri.<br>
    - Пример запроса: ```yandex/models/models_all/%category_id%?geo_id=geo_id&result_type=models&-11=%vendor_id%&fields=MODEL_VENDOR```

12) **models_search**
    - ```models_search``` - получение списка моделей по строке.<br>
    - Параметры: geo_id, result_type,-8(фильтр по стоке), fields. id категории используется в uri.<br>
    - Пример запроса: ```yandex/models/models_search?geo_id=geo_id&-8=%model_name%5&result_type=models&fields=MODEL_VENDOR```

13) **models_match**
    - ```models_match``` - поиск модели по названию.<br>
    - Параметры: name.<br>
    - Пример запроса: ```yandex/models/models_match?name=%model_name%```

14) **model_info**
    - ```model_info``` - получение подробной информации о модели.<br>
    - Параметры: geo_id. id модели используется в uri.<br>
    - Пример запроса:  ```yandex/models/model_info/%model_id%?geo_id=geo_id```

15) **models_file**
    - ```models_file``` - получение списка моделей яндекс.маркета соответствующих перечню наименований из 1С. <br>
    - Параметры: без параметров.<br>
    - Баги: Из-за циклического обращения к  api  яндекс.маркета возникают ошибки: <br>
502 Bad Gateway и cURL exception 52.
Отсутствует автоматическое определение количества строк файла для цикла. Не дорабатана часть по работе с файлами.
    - Пример запроса: ```yandex/models/models_file``` 

> В YandexController  в  setParams() необходимо указать ключ.
В методе categories_rec нет валидации параметров.